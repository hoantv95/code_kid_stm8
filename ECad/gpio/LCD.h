#ifndef		__LCD__H
#define		__LCD__H

#include "stm8s.h"

#define LCD_RS     GPIOE, GPIO_PIN_0
#define LCD_EN     GPIOD, GPIO_PIN_2
#define LCD_DB4    GPIOG, GPIO_PIN_1
#define LCD_DB5    GPIOG, GPIO_PIN_0
#define LCD_DB6    GPIOC, GPIO_PIN_4
#define LCD_DB7    GPIOC, GPIO_PIN_3
 
 void Lcd_SetBit(char data_bit);
 void Lcd_Cmd(char a);
 void Lcd_Begin(void);
 void Lcd_Print_Char(char data);
 void Lcd_Clear(void);
 void Lcd_Set_Cursor(char a, char b);
 void Lcd_Print_String(char *a);



#endif