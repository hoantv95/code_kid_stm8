#ifndef		__I2C_SOFT__H
#define		__I2C_SOFT__H

#include <iostm8s207cb.h>

#define SDA             PB_ODR_ODR5
#define SDA_IDR         PB_IDR_IDR5
#define SDA_DDR         PB_DDR_DDR5
#define SDA_CR2         PB_CR2_C25

#define SCL             PB_ODR_ODR4

#define TIME_DELAY      5


#define    ds3231_slavew  0xD0
#define    ds3231_slaver  0xD1
#define    R             1
#define    W             0




void delay(unsigned int time);
void start_i2c();
void stop_i2c();
void write_byte_i2c(unsigned char byte);
unsigned char read_byte_i2c(unsigned char address);
void write_bcd_i2c(unsigned char byte);
unsigned char read_bcd_i2c(unsigned char address);
void read_time(unsigned char *hours, unsigned char *minutes, unsigned char *seconds);
void read_date(unsigned char *w, unsigned char *dd, unsigned char *mm, unsigned char *yy);
void save_time(unsigned char h, unsigned char m, unsigned char s);
void save_date(unsigned char thu, unsigned char d, unsigned char m, unsigned char y);
void init_rtc();


#endif
