#include "LCD.h"
#include "Delay.h"


void Lcd_SetBit(char data_bit) //Based on the Hex value Set the Bits of the Data Lines
{
    if(data_bit& 1) 
        GPIO_WriteHigh(LCD_DB4); //D4 = 1
    else
        GPIO_WriteLow(LCD_DB4); //D4=0

    if(data_bit& 2)
        GPIO_WriteHigh(LCD_DB5); //D5 = 1
    else
        GPIO_WriteLow(LCD_DB5); //D5=0

    if(data_bit& 4)
        GPIO_WriteHigh(LCD_DB6); //D6 = 1
    else
        GPIO_WriteLow(LCD_DB6); //D6=0

    if(data_bit& 8) 
        GPIO_WriteHigh(LCD_DB7); //D7 = 1
    else
        GPIO_WriteLow(LCD_DB7); //D7=0
}

void Lcd_Cmd(char a)
{
    GPIO_WriteLow(LCD_RS); //RS = 0          
    Lcd_SetBit(a); //Incoming Hex value
    GPIO_WriteHigh(LCD_EN); //EN  = 1         
    //delay_ms(2);
    Delay_ms(2);
    GPIO_WriteLow(LCD_EN); //EN  = 0      
}


 
 void Lcd_Begin(void)
 {
	  
    GPIO_DeInit(GPIOE);
    GPIO_DeInit(GPIOD);
    GPIO_DeInit(GPIOG);
    GPIO_DeInit(GPIOC);
    GPIO_Init(LCD_RS, GPIO_MODE_OUT_PP_LOW_FAST);
    GPIO_Init(LCD_EN, GPIO_MODE_OUT_PP_LOW_FAST);
    GPIO_Init(GPIOE,GPIO_PIN_1,GPIO_MODE_OUT_PP_LOW_FAST);
    GPIO_Init(LCD_DB4, GPIO_MODE_OUT_PP_LOW_FAST);
    GPIO_Init(LCD_DB5, GPIO_MODE_OUT_PP_LOW_FAST);
    GPIO_Init(LCD_DB6, GPIO_MODE_OUT_PP_LOW_FAST);
    GPIO_Init(LCD_DB7, GPIO_MODE_OUT_PP_LOW_FAST);
    GPIO_Init(GPIOD,GPIO_PIN_7,GPIO_MODE_OUT_PP_LOW_FAST);
    Delay_ms(2);
    Lcd_SetBit(0x00);
    //Delay_ms(50);
    Delay_ms(100);
    Lcd_Cmd(0x03);
    Delay_ms(1);
    Lcd_Cmd(0x03);
    Delay_ms(1);
// Them delay
    Lcd_Cmd(0x03); 
  //  Delay_ms(1);
    Lcd_Cmd(0x02); //02H is used for Return home -> Clears the RAM and initializes the LCD
    Lcd_Cmd(0x02); //02H is used for Return home -> Clears the RAM and initializes the LCD
  //  Delay_ms(2);
    Lcd_Cmd(0x08); //Select Row 1
   // Delay_ms(1);
    Lcd_Cmd(0x00); //Clear Row 1 Display
   // Delay_ms(1);
    Lcd_Cmd(0x0C); //Select Row 2
   // Delay_ms(1);
    Lcd_Cmd(0x00); //Clear Row 2 Display
  //  Delay_ms(2);
    Lcd_Cmd(0x06);
  //  Delay_ms(1);
 }
 

void Lcd_Clear(void)
{
    Lcd_Cmd(0); //Clear the LCD
    Lcd_Cmd(1); //Move the curser to first position
}


void Lcd_Set_Cursor(char a, char b)
{
    char temp,z,y;
    if(a== 1)
    {
      temp = 0x80 + b - 1; //80H is used to move the curser
        z = temp>>4; //Lower 8-bits
        y = temp & 0x0F; //Upper 8-bits
        Lcd_Cmd(z); //Set Row
        Lcd_Cmd(y); //Set Column
    }
    else if(a== 2)
    {
        temp = 0xC0 + b - 1;
        z = temp>>4; //Lower 8-bits
        y = temp & 0x0F; //Upper 8-bits
        Lcd_Cmd(z); //Set Row
        Lcd_Cmd(y); //Set Column
    }
    else if(a== 3)
     {
       temp = 0x90 + b - 1;
        z = temp>>4; //Lower 8-bits
        y = temp & 0x0F; //Upper 8-bits
        Lcd_Cmd(z); //Set Row
        Lcd_Cmd(y); //Set Column
     }
    else if(a== 4)
     {
       temp = 0xD0 + b - 1;
        z = temp>>4; //Lower 8-bits
        y = temp & 0x0F; //Upper 8-bits
        Lcd_Cmd(z); //Set Row
        Lcd_Cmd(y); //Set Column
     }
       
}

 void Lcd_Print_Char(char data)  //Send 8-bits through 4-bit mode
{
   char Lower_Nibble,Upper_Nibble;
   Lower_Nibble = data&0x0F;
   Upper_Nibble = data&0xF0;
   GPIO_WriteHigh(LCD_RS);             // => RS = 1
   Lcd_SetBit(Upper_Nibble>>4);             //Send upper half by shifting by 4
   GPIO_WriteHigh(LCD_EN); //EN = 1
   Delay_ms(1);
   GPIO_WriteLow(LCD_EN); //EN = 0
   Lcd_SetBit(Lower_Nibble); //Send Lower half
   GPIO_WriteHigh(LCD_EN); //EN = 1
   Delay_ms(1);
   GPIO_WriteLow(LCD_EN); //EN = 0
}

void Lcd_Print_String(char *a)
{
    int i;
    for(i=0;a[i]!='\0';i++)
       Lcd_Print_Char(a[i]);  //Split the string using pointers and call the Char function 
}


