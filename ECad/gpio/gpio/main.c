#include "stm8s.h"
#include "lcd_txt.h"

//volatile uint32_t time_keeper=0;
void IO_Config()
{
  GPIO_DeInit(GPIOE);
  GPIO_DeInit(GPIOC);
  GPIO_DeInit(GPIOG);
  GPIO_DeInit(GPIOD);
  GPIO_Init(GPIOE,GPIO_PIN_0,GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(GPIOE,GPIO_PIN_1,GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(GPIOC,GPIO_PIN_3,GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(GPIOC,GPIO_PIN_4,GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(GPIOG,GPIO_PIN_0,GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(GPIOG,GPIO_PIN_1,GPIO_MODE_OUT_PP_LOW_FAST);
 // GPIO_Init(GPIOD,GPIO_PIN_2,GPIO_MODE_OUT_PP_LOW_FAST);
}


void CLK_Config()
{
  CLK_DeInit();
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // f_Master = HSI/1 = 16 Mhz
  CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1); // f_CPU =f_Master = HSI/1 = 16 Mhz
  while(CLK_GetFlagStatus(CLK_FLAG_HSIRDY) != SET); // wait until HSI ready
}

void TIM4_Config()
{
 TIM4_TimeBaseInit(TIM4_PRESCALER_128,124);
  TIM4_ClearFlag(TIM4_FLAG_UPDATE);
 TIM4_ITConfig(TIM4_IT_UPDATE,ENABLE);
 enableInterrupts();
  TIM4_Cmd(ENABLE);

 }


void main()
{
  CLK_Config();
  IO_Config();
  TIM4_Config();
 
  lcd_init();
  lcd_puts(0,0,(int8_t*)"Hello World");

  while(1)
  {
  // GPIO_WriteReverse(GPIOD,GPIO_PIN_7);
  // Delay_ms(1000);
    lcd_puts(0,0,(int8_t*)"Hello World");
  }
}

void assert_failed(u8* file, u32 line) 
{ 
  while (1)
  {
  }
} 