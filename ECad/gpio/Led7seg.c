#include "Led7seg.h"
#include "Delay.h"

/* Private Variables ---------------------------------------------------------*/
uint8_t ledCode[10] = {0x81,0xB7,0xC2,0x92,0xB4,0x98,0x88,0xB3,0x80,0x90};
uint8_t led1=0, led2=0,led3=0, led4=0, led5=0, led6=0, led7=0, led8=0;
unsigned char buffer[8];

void Led7segConfig(void)
{
	GPIO_Init(SCLK_PORT, 	SCLK_PIN, GPIO_MODE_OUT_PP_HIGH_FAST); 
	GPIO_Init(SDAT_PORT, 	SDAT_PIN, GPIO_MODE_OUT_PP_HIGH_FAST); 
	GPIO_Init(SLCH_PORT, 	SLCH_PIN, GPIO_MODE_OUT_PP_HIGH_FAST);  
}

void shifterWrite(uint8_t sdata)
{
  uint8_t i, temp;	
  temp = sdata;
  for(i = 0; i<8; i++)
  {
    if(temp & 0x80)
		{
      SDAT_ON;
		}
    else
		{
      SDAT_OFF;
		}
    
    shifterClock();
    temp = temp << 1;
  }
}
// Viet them
void E4094_Out(unsigned char *p, unsigned char n)
{
  unsigned char j,b;
  for(j=0;j<n;j++)
  {
    b = *(p+n-j-1);
    shifterWrite(b);
  }
  shifterLatch();
}
//
void shifterTransmit(uint8_t *arrData)
{		
	while(*arrData)
	{
		shifterWrite(*arrData);
		arrData++;
	}
		
	shifterLatch();
}

void shifterClock(void)
{
  SCLK_ON;
  Delay_ms(1);
  SCLK_OFF;
}

void shifterLatch(void)
{
  SLCH_ON;
  Delay_ms(1);
  SLCH_OFF;
}

void display(uint8_t pos, uint8_t num)
{	
	if(num <= 9 && num >=0)
	{
		switch(pos)
		{
			case 1: led1 = num; break;
			case 2: led2 = num; break;
			case 3: led3 = num; break;
			case 4: led4 = num; break;
			case 5: led5 = num; break;
			case 6: led6 = num; break;
			case 7: led7 = num; break;
			case 8: led8 = num; break;
		}
	}
	
	shifterWrite(ledCode[led8]);
	shifterWrite(ledCode[led7]);
	shifterWrite(ledCode[led6]);
	shifterWrite(ledCode[led5]);
	shifterWrite(ledCode[led4]);
	shifterWrite(ledCode[led3]);
	shifterWrite(ledCode[led2]);
	shifterWrite(ledCode[led1]);
	
	shifterLatch();	
}

void Hienthiso(unsigned long data)
{
   buffer[0]= ledCode[data/10000000];
   buffer[1]= ledCode[data%10000000/1000000];
   buffer[2]= ledCode[data%1000000/100000];
   buffer[3]= ledCode[data%100000/10000];
   buffer[4]= ledCode[data%10000/1000];  
   buffer[5]= ledCode[data%1000/100];
   buffer[6]= ledCode[data%100/10];
   buffer[7]= ledCode[data%10];
   E4094_Out( buffer,8);
   Delay_ms(400);
}